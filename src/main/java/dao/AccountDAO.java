package dao;

import domain.Account;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO {
    private static final String CREATE =
            "CREATE TABLE IF NOT EXISTS accounts (" +
            "username VARCHAR(50) PRIMARY KEY," +
            "password VARCHAR(50) NOT NULL," +
            "balance INT DEFAULT 0 NOT NULL);";
    private static final String SELECT_ALL = "SELECT * FROM accounts;";
    private static final String SELECT_BY_USERNAME = "SELECT * FROM accounts WHERE username = ?;";
    private static final String INSERT = "INSERT INTO accounts (username, password, balance) VALUES(?, ?, ?);";
    private static final String UPDATE = "UPDATE accounts SET password = ?, balance = ? WHERE username = ?;";
    private static final String DELETE_ALL = "DELETE FROM accounts;";

    public static void createTable() {
        try (Connection connection = ConnectionFactory.getConnection();
            Statement statement = connection.createStatement()) {
            statement.execute(CREATE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Account> getAll() {
        List<Account> accountList = new ArrayList<>();
        try (Connection conn = ConnectionFactory.getConnection();
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(SELECT_ALL)) {

            while (rs.next()) {
                String username = rs.getString("username");
                String password = rs.getString("password");
                int balance = rs.getInt("balance");
                accountList.add(new Account(username, password, balance));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountList;
    }

    public Account getByUsername(String username) {
        Account account = null;
        try (Connection conn = ConnectionFactory.getConnection();
            PreparedStatement statement = conn.prepareStatement(SELECT_BY_USERNAME)) {
            statement.setString(1, username);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String db_username = rs.getString("username");
                String password = rs.getString("password");
                int balance = rs.getInt("balance");
                account = new Account(db_username, password, balance);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return account;
    }

    public boolean insert(Account account) {
        boolean inserted = false;
        try (
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement statement = conn.prepareStatement(INSERT)) {
            statement.setString(1, account.getUsername());
            statement.setString(2, account.getPassword());
            statement.setInt(3, account.getBalance());
            inserted = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inserted;
    }

    public boolean update(Account account) {
        boolean updated = false;
        try (Connection conn = ConnectionFactory.getConnection();
            PreparedStatement statement = conn.prepareStatement(UPDATE)) {
            statement.setString(1, account.getPassword());
            statement.setInt(2, account.getBalance());
            statement.setString(3, account.getUsername());
            updated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void clear() {
        try (Connection conn = ConnectionFactory.getConnection();
            Statement statement = conn.createStatement()) {
            statement.execute(DELETE_ALL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}