package dao;

import domain.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {
    private static final String CREATE =
            "CREATE TABLE IF NOT EXISTS products (" +
                    "id SERIAL PRIMARY KEY," +
                    "name VARCHAR(50) NOT NULL," +
                    "price INT DEFAULT 0 NOT NULL," +
                    "quantity INT DEFAULT 0 NOT NULL);";
    private static final String SELECT_ALL = "SELECT * FROM products;";
    private static final String SELECT_BY_ID = "SELECT * FROM products WHERE id = ?;";
    private static final String INSERT = "INSERT INTO products (name, price, quantity) VALUES(?, ?, ?);";
    private static final String UPDATE = "UPDATE products SET name = ?, price = ?, quantity = ? WHERE id = ?;";
    private static final String DELETE_ALL = "DELETE FROM products;";

    public static void createTable() {
        try (Connection connection = ConnectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(CREATE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Product> getAll() {
        List<Product> productList = new ArrayList<>();
        try (Connection conn = ConnectionFactory.getConnection();
             Statement statement = conn.createStatement();
             ResultSet rs = statement.executeQuery(SELECT_ALL)) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                int quantity = rs.getInt("quantity");
                productList.add(new Product(id, name, price, quantity));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }

    public Product getById(int prod_id) {
        Product product = null;
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement statement = conn.prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, prod_id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                int quantity = rs.getInt("quantity");
                product = new Product(id, name, price, quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public boolean insert(Product product) {
        boolean inserted = false;
        try (
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement statement = conn.prepareStatement(INSERT)) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getPrice());
            statement.setInt(3, product.getQuantity());
            inserted = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inserted;
    }

    public boolean update(Product product) {
        boolean updated = false;
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement statement = conn.prepareStatement(UPDATE)) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getPrice());
            statement.setInt(3, product.getQuantity());
            statement.setInt(4, product.getId());
            updated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void clear() {
        try (Connection conn = ConnectionFactory.getConnection();
             Statement statement = conn.createStatement()) {
            statement.execute(DELETE_ALL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
