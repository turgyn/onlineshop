package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    private static final String URL = "jdbc:postgresql://localhost:5432/online_shop_db";
    private static final String USER = "darustur";
    private static final String PASSWORD = "darustur";
    private static boolean loaded = false;

    public static Connection getConnection() {
        Connection conn = null;
        try {
            if (!loaded) {
                Class.forName("org.postgresql.Driver");
                loaded = true;
            }
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

}
