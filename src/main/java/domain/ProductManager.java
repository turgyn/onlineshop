package domain;

import dao.ProductDAO;

import java.util.List;

public class ProductManager {
    private static ProductDAO productDAO;
    private static ProductManager productManager;

    public static ProductManager getManager() {
        if (productManager == null) {
            productManager = new ProductManager();
            productDAO = new ProductDAO();
        }
        return productManager;
    }

    public String getProducts() {
        List<Product> productList = productDAO.getAll();
        if (productList.isEmpty()) return "Empty";
        StringBuilder builder = new StringBuilder();
        for (Product product: productList) {
            builder.append(product).append('\n');
        }
        return builder.toString();
    }

    public void addProduct(Product product) {
        productDAO.insert(product);
    }

    // Fake products
    public void fillTestProds() {
        for (int i = 0; i < 10; i++) {
            Product product = new Product(String.valueOf(i*890), i * 999, i);
            productDAO.insert(product);
        }
    }

    public void clearTable() {
        productDAO.clear();
    }
}
