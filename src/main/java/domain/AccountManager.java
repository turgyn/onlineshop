package domain;

import dao.AccountDAO;
import dao.ProductDAO;
import exceptions.*;

import java.util.List;

public class AccountManager {
    private static AccountDAO accountDAO;
    private static AccountManager accountManager;

    public static AccountManager getManager() {
        if (accountManager == null) {
            accountManager = new AccountManager();
            accountDAO = new AccountDAO();
        }
        return accountManager;
    }

    public void authenticate(Account account)
            throws UserNotFoundException, WrongPasswordException {
        if (!isValidPassword(account.getPassword())) {
            throw new WrongPasswordException(
                    "Password should contain:\nUpper case letter\nLower case letter\nSpecific symbol\nDigit");
        }
        account.setPassword(hashPass(account.getPassword()));
        Account checker = accountDAO.getByUsername(account.getUsername());
        if (checker == null) {
            throw new UserNotFoundException(account.getUsername());
        }
        if (!checker.getPassword().equals(account.getPassword())) {
            throw new WrongPasswordException("Wrong password");
        } else {
            account.setBalance(checker.getBalance());
        }
    }

    public void authorize(Account account) throws UsernameAlreadyExists, WrongPasswordException {
        if (!isValidPassword(account.getPassword())) {
            throw new WrongPasswordException(
                    "Password should contain:\nUpper case letter\nLower case letter\nSpecific symbol\nDigit");
        }
        account.setPassword(hashPass(account.getPassword()));
        System.out.println(account.getUsername());
        Account checker = accountDAO.getByUsername(account.getUsername());
        if (checker != null) {
            throw new UsernameAlreadyExists(account.getUsername());
        }
        accountDAO.insert(account);
    }

    public String getAccounts() {
        List<Account> accountList = accountDAO.getAll();
        if (accountList.isEmpty()) return "Empty";
        StringBuilder builder = new StringBuilder();
        for (Account account: accountList) {
            builder.append(account).append('\n');
        }
        return builder.toString();
    }

    public void purchase(Account account, int prod_id, int prod_amount)
            throws NotEnoughMoneyException, ProductNotFoundExcetpion, NotEnoughProductException {
        ProductDAO productDAO = new ProductDAO();
        Product product = productDAO.getById(prod_id);
        if (product == null) {
            throw new ProductNotFoundExcetpion(prod_id);
        }
        if (product.getQuantity() < prod_amount) {
            throw new NotEnoughProductException(product.getQuantity());
        }
        if (account.getBalance() - product.getPrice() * prod_amount < 0) {
            throw new NotEnoughMoneyException(account.getBalance(), product.getPrice());
        }
        account.setBalance(account.getBalance() - product.getPrice() * prod_amount);
        product.setQuantity(product.getQuantity() - prod_amount);
        accountDAO.update(account);
        productDAO.update(product);
    }

    public void updatePassword(Account account, String old_pass, String new_pass) throws WrongPasswordException {
        old_pass = hashPass(old_pass);
        if (!account.getPassword().equals(old_pass)) {
            throw new WrongPasswordException("Wrong old password");
        }
        if (!isValidPassword(new_pass)) {
            throw new WrongPasswordException(
                    "Password should contain:\nUpper case letter\n" +
                    "Lower case letter\nSpecific symbol\nDigit");
        }
        account.setPassword(hashPass(new_pass));
        accountDAO.update(account);
    }

    public void topUpBalance(Account account, int amount) {
        account.setBalance(account.getBalance() + amount);
        accountDAO.update(account);
        accountDAO.getByUsername(account.getUsername());
    }

    private boolean isValidPassword(String password) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
        return password.matches(pattern);
    }

    // Fake password hasher
    private static String hashPass(String pass) {
        int hashCode = Math.abs(pass.hashCode());
        StringBuilder builder = new StringBuilder();
        while (hashCode > 0) {
            int p = hashCode % 62;
            hashCode /= 62;
            char cur;
            if (p < 26) cur = (char) ('A' + p);
            else if (p < 52) cur = (char) ('a' + p - 26);
            else cur = (char) ('0' + p - 52);
            builder.append(cur);
        }
        return builder.toString();
    }

    public void clearTable() {
        accountDAO.clear();
    }

}
