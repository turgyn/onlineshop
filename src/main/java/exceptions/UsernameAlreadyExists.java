package exceptions;

public class UsernameAlreadyExists extends Exception {
    public UsernameAlreadyExists(String username) {
        super("Username: " + username + " already exists");
    }
}
