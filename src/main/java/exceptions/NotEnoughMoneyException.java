package exceptions;

public class NotEnoughMoneyException extends Exception{
    public NotEnoughMoneyException(int balance, int productPrice) {
        super("Account balance: " + balance + "\nProduct Price: " + productPrice);
    }
}
