package exceptions;

public class ProductNotFoundExcetpion extends  Exception {
    public ProductNotFoundExcetpion(int id) {
        super("Product with id: " + id + " not found");
    }
}
