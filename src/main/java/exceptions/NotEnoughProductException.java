package exceptions;

public class NotEnoughProductException extends Exception {
    public NotEnoughProductException(int prod_quant) {
        super("Only " + prod_quant + " prods in store");
    }
}
