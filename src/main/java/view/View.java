package view;

import domain.AccountManager;
import domain.ProductManager;

import java.util.Scanner;

public abstract class View {
    protected AccountManager accountManager;
    protected ProductManager productManager;
    protected String starterMessage = "Choose operation number";
    protected Scanner scanner;

    public View() {
        accountManager = AccountManager.getManager();
        productManager = ProductManager.getManager();
        scanner = new Scanner(System.in);
    }

    public abstract void execute();

    protected void loop(String[] menu) {
        while (true) {
            String operation = clientInteraction(menu);
            if (!controller(operation))
                break;
        }
    }

    private String clientInteraction(String[] menu) {
        System.out.println(starterMessage);
        for (String row: menu) {
            System.out.println(row);
        }
        return scanner.next();
    }

    protected abstract boolean controller(String operation);
}
