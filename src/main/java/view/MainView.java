package view;

import dao.AccountDAO;
import dao.ProductDAO;
import domain.Account;
import exceptions.*;

public class MainView extends View {
    private final String[] MENU = new String[]{
            "[1]: Log In",
            "[2]: Sign Up",
            "[3]: Show products",
            "[4]: Exit",
            "[For Tests] or future store administration",
            "[5]: Users List",
            "[6]: Product Menu",
            "[7]: Clear all tables"
    };

    @Override
    public void execute() {
        ProductDAO.createTable();
        AccountDAO.createTable();
        loop(MENU);
    }

    @Override
    protected boolean controller(String operation) {
        Account loggedIn = null;
        int numOper;
        try {
            numOper = Integer.parseInt(operation);
        } catch (NumberFormatException e) {
            numOper = 0;
        }
        switch (numOper) {
            case 1:
                loggedIn = authenticate();
                break;
            case 2:
                loggedIn = authorize();
                break;
            case 3:
                System.out.println(productManager.getProducts());
                break;
            case 4:
                System.out.println("Bye");
                return false;
            case 5:
                System.out.println(accountManager.getAccounts());
                break;
            case 6:
                ProductView productView = new ProductView();
                productView.execute();
                break;
            case 7:
                accountManager.clearTable();
                productManager.clearTable();
                break;
            default:
                System.err.println("No such command");
        }
        if (loggedIn != null) {
            AccountView accountView = new AccountView(loggedIn);
            accountView.execute();
        }
        return true;
    }

    public Account authenticate() {
        Account account = new Account();
        System.out.print("Username: ");
        account.setUsername(scanner.next());
        System.out.print("Password: ");
        account.setPassword(scanner.next());
        try {
            accountManager.authenticate(account);
            System.out.println("VIEW: " + account);
        } catch (WrongPasswordException | UserNotFoundException e) {
            System.err.println(e.toString());
            return null;
        }
        System.out.println("Welcome");
        return account;
    }

    public Account authorize() {
        Account account = new Account();
        System.out.print("Username: ");
        account.setUsername(scanner.next());
        System.out.print("Password: ");
        String pass1 = scanner.next();
        System.out.print("Confirm Password: ");
        String pass2 = scanner.next();
        try {
            if (!pass1.equals(pass2))
                throw new WrongPasswordException("Password Confirmation Failed");
            account.setPassword(pass1);
            accountManager.authorize(account);
        } catch (UsernameAlreadyExists | WrongPasswordException e) {
            System.err.println(e.toString());
            return null;
        }
        System.out.println("New User created");
        return account;
    }
}
