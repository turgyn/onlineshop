package view;

import domain.Product;

public class ProductView extends View {
    private final String[] MENU = new String[]{
            "[1]: Show products",
            "[2]: Add new Product",
            "[3]: fill db with test products",
            "[4]: Exit",
    };

    @Override
    public void execute() {
        loop(MENU);
    }

    @Override
    protected boolean controller(String operation) {
        int numOper;
        try {
            numOper = Integer.parseInt(operation);
        } catch (NumberFormatException e) {
            numOper = 0;
        }
        switch (numOper) {
            case 1:
                System.out.println(productManager.getProducts());
                break;
            case 2:
                newProduct();
                break;
            case 3:
               productManager.fillTestProds();
                break;
            case 4:
                return false;
            default:
                System.err.println("No such command");
        }
        return true;
    }

    private void newProduct() {
        System.out.print("Name: ");
        String name = scanner.next();
        System.out.print("Price: ");
        int price = scanner.nextInt();
        System.out.print("Amount: ");
        int amount = scanner.nextInt();
        productManager.addProduct(new Product(name, price, amount));
        System.out.println("Product: " + name + " added");
    }
}
