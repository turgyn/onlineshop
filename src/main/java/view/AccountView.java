package view;

import domain.Account;
import exceptions.*;

public class AccountView extends View {
    protected Account account;
    private final String[] MENU = new String[]{
            "[1]: User Info",
            "[2]: Product List",
            "[3]: Make a purchase",
            "[4]: Top up Balance",
            "[5]: Change Password",
            "[5]: Exit"
    };

    public AccountView(Account account) {
        this.account = account;
    }

    @Override
    public void execute() {
        loop(MENU);
    }

    @Override
    protected boolean controller(String operation) {
        int numOper;
        try {
            numOper = Integer.parseInt(operation);
        } catch (NumberFormatException e) {
            numOper = 0;
        }
        switch (numOper) {
            case 1:
                System.out.println(account);
                break;
            case 2:
                System.out.println(productManager.getProducts());
                break;
            case 3:
                purchase();
                break;
            case 4:
                System.out.print("Enter amount: ");
                int amount = scanner.nextInt();
                accountManager.topUpBalance(account, amount);
                System.out.println("+" + amount + "tg");
                break;
            case 5:
                changePassword();
                break;
            case 6:
                return false;
            default:
                System.err.println("No such command");
        }
        return true;
    }

    private void purchase() {
        System.out.println("Purchase Menu");
        System.out.print("Enter product id: ");
        int prod_id = scanner.nextInt();
        System.out.print("Enter quantity: ");
        int quantity = scanner.nextInt();
        try {
            accountManager.purchase(account, prod_id, quantity);
        } catch (NotEnoughMoneyException | NotEnoughProductException | ProductNotFoundExcetpion e) {
            System.err.println(e.toString());
        }
    }

    private void changePassword() {
        System.out.print("Old Password: ");
        String oldPass = scanner.next();
        System.out.print("New Password: ");
        String pass = scanner.next();
        System.out.print("Confirm New Password: ");
        String pass2 = scanner.next();

        try {
            if (!pass.equals(pass2))
                throw new WrongPasswordException("Password Confirmation Failed");
            accountManager.updatePassword(account, oldPass, pass);
        } catch (WrongPasswordException e) {
            System.err.println(e.toString());
        }
    }
}
